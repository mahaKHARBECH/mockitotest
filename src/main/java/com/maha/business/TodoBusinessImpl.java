package com.maha.business;

import java.util.ArrayList;
import java.util.List;

import com.maha.api.TodoService;


//TodoBusinessImpl  SUT system under test
//TodoService dependency
public class TodoBusinessImpl {
	
	private TodoService todoService;

	public TodoBusinessImpl(TodoService todoService) {
		super();
		this.todoService = todoService;
	}
	
	public List<String> retrieveTodoRelatedToSpring (String user)  {
	
		List<String> filteredTodos = new ArrayList<String>();
		List<String> todos = todoService.retieveTodos(user);
		for (String todo : todos) {
			if (todo.contains("Spring")) {
				filteredTodos.add(todo);
			}
		}
		return filteredTodos;
	}

}
