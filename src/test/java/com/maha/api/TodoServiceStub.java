package com.maha.api;

import java.util.Arrays;
import java.util.List;

public class TodoServiceStub implements TodoService{

	public List<String> retieveTodos(String user) {

		// sample implementation 
		return Arrays.asList("Learn Spring MVC", "Learn Spring",
				"Learn to dance");
	}

}
