package com.maha.business;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.maha.api.TodoService;
import com.maha.api.TodoServiceStub;

public class TodoBusinessImplTestStub {

	@Test
	public void testRetrieveTodoRelatedToSpring() {
		TodoService todoServiceStub = new TodoServiceStub();
		TodoBusinessImpl businessImpl = new TodoBusinessImpl(todoServiceStub);
		
		List<String> filteredTodos = businessImpl.retrieveTodoRelatedToSpring("Dummy");
		assertEquals(2, filteredTodos.size());
	}

}
