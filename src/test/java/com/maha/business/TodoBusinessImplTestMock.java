package com.maha.business;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.maha.api.TodoService;


public class TodoBusinessImplTestMock {

	@Test
	public void testRetrieveTodoRelatedToSpring_usingAMock() {
		
		TodoService todoServiceMock = mock(TodoService.class);
		List<String> todos = Arrays.asList("Learn Spring MVC", "Learn Spring",
				"Learn to dance");
		when(todoServiceMock.retieveTodos("Dummy")).thenReturn(todos);
		
		TodoBusinessImpl businessImpl = new TodoBusinessImpl(todoServiceMock);
		
		List<String> filteredTodos = businessImpl.retrieveTodoRelatedToSpring("Dummy");
		assertEquals(2, filteredTodos.size());
	}
	
	@Test
	public void testRetrieveTodoRelatedToSpring_withEmptyList() {
		
		TodoService todoServiceMock = mock(TodoService.class);
		List<String> todos = Arrays.asList();
		when(todoServiceMock.retieveTodos("Dummy")).thenReturn(todos);
		
		TodoBusinessImpl businessImpl = new TodoBusinessImpl(todoServiceMock);
		
		List<String> filteredTodos = businessImpl.retrieveTodoRelatedToSpring("Dummy");
		assertEquals(0, filteredTodos.size());
	}

}
