package com.maha.business;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Test;

public class ListTest {

	@Test
	public void testListSizeMock() {
		List listMock = mock(List.class);
		when(listMock.size()).thenReturn(2);
		
		assertEquals(2, listMock.size());
		
	}
	

	@Test
	public void testListSizeMock_MultipleValues() {
		List listMock = mock(List.class);
		when(listMock.size()).thenReturn(2).thenReturn(3);
		
		assertEquals(2, listMock.size());
		assertEquals(3, listMock.size());
		
	}
	
	
	@Test
	public void testListSizeMockGet() {
		List listMock = mock(List.class);
		when(listMock.get(0)).thenReturn("maha");
		
		assertEquals("maha", listMock.get(0));
		assertEquals(null, listMock.get(1));
	
	}
	
	
	@Test
	public void testListSizeMockGetSameValue() {
		List listMock = mock(List.class);
		//Argument Matcher
		when(listMock.get(anyInt())).thenReturn("maha");
		
		assertEquals("maha", listMock.get(0));
		assertEquals("maha", listMock.get(1));
	}
	
	
	@Test(expected=RuntimeException.class)
	public void testListSizeMockGetSameValue_ThrowAnException() {
		List listMock = mock(List.class);
		//Argument Matcher
		when(listMock.get(anyInt())).thenThrow(new RuntimeException("Stop!!"));
		
		listMock.get(0);
	
	}
}
